variable "project" {}

variable "credentials_file" {}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-a"
}

variable "os_image" {
  default = "cos-cloud/cos-stable"
  description = "This is a description from the end user's perspective"
}

#Defining a tuple for a machine type
variable "vm_params" {
  type = tuple([string, string, bool])
  description = "vm parameters"
  default = ["f1-micro", "us-central1-a", true]
}

#Map and Object
#Object => Collection of named attributes, each with their own type.
#Map => contains elements of the same type

#Map
variable "my_map" {
  type = map(string)
  description = "GCP provider params"
  default = {
    project = "projectID"
    credentials = "cred.json"
    region = "us-central1"
  }
}

#Object
variable "my_vm_params" {
  type = object({
    name = string
    machine_type = string
    zone = string
    allow_stopping_for_update = bool
    #Adding an object into an object
    disk = object({
      source_image = string
      auto_delete = bool
      boot = bool
    })
  })

  default = {
    name = "appserver"
    machine_type = "f1-micro"
    zone = "us-central1-a"
    allow_stopping_for_update = true
    disk = {
      source_image = "cos-cloud/cos-stable"
      auto_delete = true
      boot= true
    }
  }

  #Custom validation_rules
  validation {
    condition =  length(var.my_vm_params.name)>3 # an expression returning true or false
    error_message = "VM name must be at least 4 characters." #Message that explains what's not working ...
  }
}
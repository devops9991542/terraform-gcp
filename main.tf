provider "google" {
  project     = var.project #"digital-gearing-412015"
  credentials = file(var.credentials_file) #file("credentials.json")
  region      = var.region
  zone        = var.zone
}

resource "google_compute_instance" "my_VM_instance" {
  name                      = var.my_vm_params.name
  machine_type              = var.my_vm_params.machine_type
  zone                      = var.my_vm_params.zone
  allow_stopping_for_update = var.my_vm_params.allow_stopping_for_update

  boot_disk {
    initialize_params {
      #image = "debian-cloud/debian-10-buster-v20220719"
      #image = "cos-cloud/cos-stable" #cos = gcp optimized-OS
      image = var.os_image

    }
  }

  network_interface {
    network    = google_compute_network.my_terraform_network.self_link # the network we just define
    subnetwork = google_compute_subnetwork.terraform_subnet.self_link  # the subnet we just defined
    # a resource's self_link => the unique reference to that resource
    access_config {
      // necessary, even empty
    }
  }
}

resource "google_compute_network" "my_terraform_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = false # We gonna do it by our own (cauz there are too many subnets related to the Default VPC_network)
}

resource "google_compute_subnetwork" "terraform_subnet" {
  name          = "terraform-subnetwork"
  ip_cidr_range = "10.10.0.0/16"
  region        = "us-central1"
  network       = google_compute_network.my_terraform_network.id
}
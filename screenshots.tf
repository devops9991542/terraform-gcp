# For variables and types purposes



#tuple elements are unmutable after being created
#list elements are mutable after their creation
variable "my_list" {
  type = list # tuple or a set
  description = "this is a list of value"
  default = [
    "A",
    42,
    true,
    "no"
  ]
}

variable "my_new_list" {
  type = list(string) #Will convert every element into String (If not yet DONE)
  description = "This is a list of Strings"
  default = [ 12, false, "hellooo" ]
}

variable "my_tuple" {
  type = tuple([ string, number, bool ])
  description = "This is a tuple"
  default = ["hellooo", 8, true]
}

variable "my_set" {
  type = set(number)
  description = "This is a set of numbers"
  default = [12, 13, 8, 19]
}
